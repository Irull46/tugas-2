import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import logo from '../assets/Youtube.png';
import sc from '../assets/Screencast.png'
import notif from '../assets/Notif.png';
import search from '../assets/Search.png';
import user from '../assets/User.jpg';

const Header = () => {
  return (
    <View style={model.view}>
      <Image source={logo} style={model.logo} />
      <Image source={sc} style={model.sc} />
      <Image source={notif} style={model.notif} />
      <Image source={search} style={model.search} />
      <Image source={user} style={model.user} />
    </View>
  );
};

const model = StyleSheet.create({
  view: {
    backgroundColor: 'white',
    width: 400,
    height: 48,
    flexDirection: 'row',
  },
  logo: {
    marginTop: 14,
    marginLeft: 16,
    width: 90,
    height: 20,
  },
  sc: {
    marginTop: 10,
    marginLeft: 78,
    width: 23,
    height: 30,
  },
  notif: {
    marginTop: 14,
    marginLeft: 27,
    width: 18,
    height: 20,
  },
  search: {
    marginTop: 11.5,
    marginLeft: 20,
    width: 27,
    height: 27,
  },
  user: {
    marginTop: 10,
    marginLeft: 19,
    width: 26,
    height: 26,
    borderRadius: 20,
  },
});

export default Header;