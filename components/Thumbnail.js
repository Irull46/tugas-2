import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import thumb from '../assets/Thumb1.jpg';
import user from '../assets/User.jpg';
import titik from '../assets/Titik.png';


const Thumbnail = () => {
    return (
        <View style={model.box1}>
            <Image source={thumb} style={model.thumb} />
            <View style={model.box2}>
                <Image source={user} style={model.user} />
                <View style={model.box3}>
                    <Text style={model.h1}>REACT NATIVE UPDATE 2021</Text>
                    <Text style={model.h5}>IDV Tech 5,9 rb x ditonton 2 hari yang lalu</Text>
                </View>
                <Image source={titik} style={model.titik} />
            </View>
        </View>
    );
};

const model = StyleSheet.create({
    box1: {
        backgroundColor: 'white',
    },
    thumb: {
        width: '100%',
        height: 200,
    },
    box2: {
        flexDirection: 'row',
        marginTop: 10,
    },
    user: {
        width: 38,
        height: 38,
        borderRadius: 20,
        marginLeft: 12,
    },
    box3: {
        flexDirection: 'column',
        marginLeft: 10,
        marginBottom: 20,
    },
    h1: {
        fontSize: 18,
    },
    h5: {
        fontSize: 12,
    },
    titik: {
        marginTop: 9,
        marginLeft: 50,
        width: 10,
        height: 10,
    },
});

export default Thumbnail;