import React from 'react';
import {ScrollView, View} from 'react-native';
import Head from './components/Header';
import Thumb from './components/Thumbnail';

const App = () => {
  return (
    <View>
      <Head />
      <ScrollView>
        <Thumb />
        <Thumb />
        <Thumb />
        <Thumb />
        <Thumb />
        <Thumb />
      </ScrollView>
    </View>
  );
};

export default App;